---
title: Adding targeted testing for merge requests
linkTitle: Adding subsystem testing
description: >-
  How to configure additional testing for a specific subsystem or kernel source
  files
weight: 80
---

`Targeted testing` is the term used to refer to CKI tests that are configured
to run only for merge requests that affect specific subsystems or kernel source
files.

## Adding targeted testing per subsystem

1. Determine the GitLab merge request label for the subsystem from the
   [owners.yaml] file, or by looking at suitable merge requests.

2. Determine whether the test you want to run is already onboarded to
   [kpet-db]. If not, talk to your friendly QE counterpart to [add the test].

3. File a merge request in [kpet-db] to enable the test for the subsystem in
   the respective test case in `cases/`.

   - To trigger the test for changes to any file that belongs to the subsystem,
     add the subsystem label prefixed with `@` to the `target_sources` key like

     ```yaml
     target_sources:
       - @mm
     ```

   - To trigger the test for subsystem files matching a certain pattern,
     append a regular expression to the subsystem label like

     ```yaml
     target_sources:
       - @mm/.*\.[ch]
     ```

   - To trigger the test for a specific subsystem file, append the file name to
     the subsystem label like

     ```yaml
     target_sources:
       - @mm/mm/page_counter\.c
     ```

   The test maintainer as specified in the test case is the best person to help
   you out if you have troubles getting things to work and for reviewing your
   merge request.

[owners.yaml]: https://gitlab.com/redhat/centos-stream/src/kernel/documentation/-/blob/main/info/owners.yaml
[add the test]: ../test-maintainers/onboarding.md

## Adding targeted testing per kernel source file

### Configuration

The configuration is done in the test case in [kpet-db]. In case some files
should trigger the test, but not be considered for targeted testing the file
should be added under `trigger_sources` list.

Example: [block tests]

```yaml
...
target_sources:
- block/blk-cgroup\.c
- block/blk-mq-debugfs\.c
- block/elevator\.c
- block/blk-mq-sched\.c
- block/blk-mq\.c
- block/blk-mq-sysfs\.c
- block/cfq-iosched\.c
- block/kyber-iosched\.c
- block/bfq-iosched\.c
- block/blk-core\.c
- block/mq-deadline\.c
trigger_sources:
- block/.*
...
```

### Missing targeted tests

Merge requests that have missing targeted tests have the
'TargetedTestingMissing' label.

#### What that means?

It means that at least 1 file updated by the merge request doesn't have a specific test
that covers it.

#### What file(s)?

The list of files missing targeted tests are in the setup jobs in the pipelines
(doesn't matter the architecture they contain the same information).

Example:

```plain
Some source files are not targeted by any tests:
fs/userfaultfd.c
include/uapi/linux/userfaultfd.h
```

#### What to do?

1. Looking at [CKI tests], maybe some test could be configured to run on that file.
It can be confirmed by running the test using code coverage.

2. Write/onboard new test case for it.

3. Talk with QE that covers this area to write/onboard new test case for it.

Don't hesitate to contact CKI team for help.

[kpet-db]: https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db/
[block tests]: https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db/-/blob/main/cases/storage/block/index.yaml
[CKI tests]: https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/
