---
title: Reproducing CKI test selection
description: >-
  How to reproduce the dynamic test selection done in the CKI pipelines
weight: 50
---

KPET is a framework which will execute targeted testing based on changes
introduced in the patch, e.g. a network driver or similar would trigger network
related testing.

## Running KPET

1. Install the [KPET] CLI

   kpet is written in Python that means you will need python3 installed on
   your system. You can execute it directly from the repository `./bin/kpet` or
   you can install it via pip and use it directly from your PATH.

   ```bash
   pip install --user git+https://gitlab.com/cki-project/kpet.git
   ```

2. Install [KPET Database]

   In order to use kpet you will need to download the kpet database which
   includes the templates and pattern files needed to analyze the patch and
   generate the corresponding Beaker XML.

   ```bash
   git clone https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db.git
   ```

3. Change directory to newly cloned kpet-db. The database is assumed to
   reside in the current working directory, otherwise add `--db DB` to specify
   the location of the kpet database.

   ```bash
   cd kpet-db
   ```

4. To preview patch generated test cases:

   ```bash
   kpet test list 001.patch
   ```

   or

   ```bash
   kpet test list https://patchwork.kernel.org/patch/10533445/mbox
   ```

5. To generate complete Beaker XML:

   ```bash
   kpet run generate \
       --description 'kpet onboarding' \
       --arch x86_64 \
       --kernel '##KPG_URL##' \
       --tree rhel8 \
       --output job.xml \
       001.patch
   ```

   or

   ```bash
   kpet run generate \
       --description 'kpet onboarding' \
       --arch x86_64 \
       --kernel '##KPG_URL##' \
       --tree rhel8 \
       --output job.xml \
       https://patchwork.kernel.org/patch/10533445/mbox
   ```

   where `##KPG_URL##` is a URL pointing to a gzipped kernel tarball with
   `/boot` and `/lib/modules/` directories or a yum repo to install the
   prebuilt RPM and dependencies.

6. To generate XML specific to a kernel subsystem, include `--sets` and the correct
   subsystem keyword:

   ```bash
   kpet run generate \
       --description 'kpet onboarding' \
       --arch x86_64 \
       --kernel '##KPG_URL##' \
       --tree rhel8 \
       --sets kt1 \
       --output kt1.xml \
       001.patch
   ```

7. Submit the job to Beaker

   ```bash
   bkr job-submit job.xml
   ```

8. Reserve the system

   Beaker CLI will display your submitted Beaker job id, you can then login to
   Beaker web UI to extend the reservation as needed.

## Complete example

```bash
pip install --user git+https://gitlab.com/cki-project/kpet
git clone https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db.git
cd kpet-db
kpet run generate \
    --db . \
    --description 'new_feature.patch' \
    --kernel 'http://your.url.here/c1b7ab.tar.gz' \
    --tree rhel8 \
    --arch x86_64 \
    --output job.xml \
    https://patchwork.kernel.org/patch/10533445/mbox
bkr job-submit job.xml
```

For complete details on how to use kpet and kpet-db, please see the [Readme].

[kpet]: https://gitlab.com/cki-project/kpet
[kpet database]: https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db
[Readme]: https://gitlab.com/cki-project/kpet/-/blob/main/README.md
