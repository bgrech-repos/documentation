---
linkTitle: Presentations
Title: Talks and presentations
description: |
  The collected list of talks and presentations including links to videos and slides
weight: 10
aliases: [/l/presentations]
---

## 2023

| Title                                                                         | Speakers                                                                  | Links                                            |
|-------------------------------------------------------------------------------|---------------------------------------------------------------------------|--------------------------------------------------|
| [A case for DAG databases][fosdem23-n-p] *@ FOSDEM 2023*                      | [Nikolai Kondrashov]                                                      | [Slides][fosdem23-n-p], [Video][fosdem23-n-v]    |
| [reflections on CKI architecture][devconf23-o-p] *@ DevConf.CZ*               | [Michael Hofmann]                                                         | [Slides][devconf23-o-s], [Source][devconf23-o-l] |
| [The Many Ways of launching AWS spot instances][devconf23-a-p] *@ DevConf.CZ* | [Michael Hofmann]                                                         | [Slides][devconf23-a-s], [Source][devconf23-a-l] |
| [Incident management for small service teams][devconf23-i-p] *@ DevConf.CZ*   | [Michael Hofmann]                                                         | [Slides][devconf23-i-s], [Source][devconf23-i-l] |
| [Kernel CI – How Far Can It Go?][eoss23-n-p] *@ EOSS 2023*                    | [Nikolai Kondrashov]                                                      | [Slides][eoss23-n-p],    [Video][eoss23-n-v]     |
| [Triaging kernel issues][qecamp23-t-s] *@ QECamp2023*                         | [Israel Santana], [Jeff Bastian], [Fendy Tjahjadi]                        | [Slides][qecamp23-t-s], [Source][qecamp23-t-l]   |
| [Mocking in Python][qecamp23-m-s] *@ QECamp2023*                              | [Ondrej Kinst], [Serhii Turivnyi]                                         | [Slides][qecamp23-m-s], [Source][qecamp23-m-l]   |
| [Testing Red Hat kernels - present and future][qecamp23-k-s] *@ QECamp2023*   | [Michael Hofmann], [Bruno Goncalves], [Israel Santana], [Tales Aparecida] | [Slides][qecamp23-k-s], [Source][qecamp23-k-l]   |
| [We have CI at home. CI at home: Git Hooks][qecamp23-h-s] *@ QECamp2023*      | [Tales Aparecida]                                                         | [Slides][qecamp23-h-s], [Source][qecamp23-h-l]   |

## 2022

| Title                                                                             | Speakers                               | Links                                                                       |
|-----------------------------------------------------------------------------------|----------------------------------------|-----------------------------------------------------------------------------|
| [Advanced GitLab CI/CD for fun and profit][devconf22-p] *@ DevConf.CZ*            | [Iñaki Malerba], [Michael Hofmann]     | [Video][devconf22-v], [Slides][devconf22-s], [Source][devconf22-l]          |
| [Masking known issues across six kernel CI systems][fosdem22-p] *@ FOSDEM*        | [Nikolai Kondrashov]                   | [Video][fosdem22-v], [Slides][fosdem22-s]                                   |
| [Keynote panel discussion: 30 years of Linux][devconf22m-v-p] *@ DevConf.CZ Mini* | [Veronika Kabatova]                    | [Video][devconf22m-v-v]                                                     |
| [Open Source requirements for Services BoF][devconf22m-m-p] *@ DevConf.CZ Mini*   | [Michael Hofmann], [Stef Walter]       | [Video][devconf22m-m-v], [Slides][devconf22m-m-s], [Source][devconf22m-m-l] |
| [How the CKI team does estimation][agile22-s]                                     | [Veronika Kabatova], [Michael Hofmann] | [Slides][agile22-s], [Source][agile22-l]                                    |

## 2021

| Title                                                                                          | Speakers                           | Links                                                                    |
|------------------------------------------------------------------------------------------------|------------------------------------|--------------------------------------------------------------------------|
| [Unifying Kernel Test Reporting with KernelCI][devconf21-n-p] *@ DevConf.CZ*                   | [Nikolai Kondrashov]               | [Video][devconf21-n-v], [Slides][devconf21-n-s], [Source][devconf21-n-l] |
| [RHEL Kernel Workflow using GitLab][devconf21-d-p] *@ DevConf.CZ*                              | [Don Zickus], [Prarit Bhargava]    | [Video][devconf21-d-v], [Slides][devconf21-d-s], [Source][devconf21-d-l] |
| [From Jenkins-under-your-desk to resilient service][devconf21-i-p] *@ DevConf.CZ*              | [Iñaki Malerba], [Michael Hofmann] | [Video][devconf21-i-v], [Slides][devconf21-i-s], [Source][devconf21-i-l] |
| [UPt! Your Provisioning of Linux Machines][devconf21-j-p] *@ DevConf.CZ*                       | [Jakub Racek]                      | [Video][devconf21-j-v], [Slides][devconf21-j-s], [Source][devconf21-j-l] |
| [Team Intro][cyborg21-i1-p] *@ Cyborg Infrastructure Workshop*                                 | [Iñaki Malerba], [Michael Hofmann] | [Slides][cyborg21-i1-s], [Source][cyborg21-i1-l]                         |
| [How the CKI team keeps its service running][cyborg21-i2-p] *@ Cyborg Infrastructure Workshop* | [Iñaki Malerba], [Michael Hofmann] | [Slides][cyborg21-i2-s], [Source][cyborg21-i2-l]                         |
| [How the CKI team hacks on its service][cyborg21-i3-p] *@ Cyborg Infrastructure Workshop*      | [Iñaki Malerba], [Michael Hofmann] | [Slides][cyborg21-i3-s], [Source][cyborg21-i3-l]                         |

## 2020

| Title                                                                         | Speakers                       | Links                                                                    |
|-------------------------------------------------------------------------------|--------------------------------|--------------------------------------------------------------------------|
| [Abusing GitLab CI to Test Kernel Patches][fosdem20-p] *@ FOSDEM*             | [Nikolai Kondrashov]           | [Video][fosdem20-v], [Slides][fosdem20-s]                                |
| [Plugging into the Red Hat kernel CI ecosystem][devconf20-d-p] *@ DevConf.CZ* | [Don Zickus]                   | [Video][devconf20-d-v], [Slides][devconf20-d-s], [Source][devconf20-d-l] |
| [7 Ways to Make Kernel Developers Like Cookies][devconf20-j-p] *@ DevConf.CZ* | [Jakub Racek], [Iñaki Malerba] | [Video][devconf20-j-v], [Slides][devconf20-j-s]                          |

## 2019

| Title                                                                          | Speakers                                  | Links                                       |
|--------------------------------------------------------------------------------|-------------------------------------------|---------------------------------------------|
| [Red Hat joins CI party, brings cookies][lp19-p] *@ Linux Plumbers Conference* | [Nikolai Kondrashov], [Veronika Kabatova] | [Video][lp19-v], [Slides][lp19-s]           |
| [Cookies for Kernel Developers][devconf19-p] *@ DevConf.CZ*                    | [Major Hayden], [Nikolai Kondrashov]      | [Video][devconf19-v], [Slides][devconf19-s] |

## 2018

| Title                                                          | Speakers     | Links                |
|----------------------------------------------------------------|--------------|----------------------|
| [Kernel CI - How Red Hat can help][devconf18-p] *@ DevConf.CZ* | [Don Zickus] | [Video][devconf18-v] |

[Bruno Goncalves]: https://gitlab.com/bgoncalv
[Don Zickus]: https://gitlab.com/dzickusrh
[Fendy Tjahjadi]: https://gitlab.com/ftjahjad
[Jakub Racek]: https://gitlab.com/jracek
[Jeff Bastian]: https://gitlab.com/jbastianrh
[Iñaki Malerba]: https://gitlab.com/inakimalerba
[Israel Santana]: https://gitlab.com/kamaxeon
[Major Hayden]: https://gitlab.com/majorhayden
[Michael Hofmann]: https://gitlab.com/mh21
[Nikolai Kondrashov]: https://gitlab.com/spbnick
[Ondrej Kinst]: https://gitlab.com/Toaster192
[Prarit Bhargava]: https://gitlab.com/prarit
[Serhii Turivnyi]: https://gitlab.com/sturivny
[Stef Walter]: https://gitlab.com/stefwalter
[Tales Aparecida]: https://gitlab.com/tales-aparecida/
[Veronika Kabatova]: https://gitlab.com/veruu

[qecamp23-m-l]: https://docs.google.com/presentation/d/16qLoOnYM2QtyEgqAlCclk3ZksuwbmE0lohC0pZvtR2M
[qecamp23-m-s]: 2023-mocking-in-python.pdf

[qecamp23-t-l]: https://docs.google.com/presentation/d/1AQf2gxkyJOqD10p7f9YZD4VZ0ccrLI-zAD1Au5cy0Ec
[qecamp23-t-s]: 2023-triaging-kernel-issues.pdf

[qecamp23-k-l]: https://docs.google.com/presentation/d/1jbfJ-gdDFfdvmFCffhlTXTjXZxkY4vbPDWerGd-rOnA
[qecamp23-k-s]: 2023-testing-red-hat-kernels.pdf

[qecamp23-h-l]: https://docs.google.com/presentation/d/1x3B4naXjK_qXnqebWUKqdQG6JURCZx6QVwQvum45cOA
[qecamp23-h-s]: 2023-git-hooks.pdf

[fosdem23-n-p]: 2023-a-case-for-dag-databases.pdf
[fosdem23-n-v]: https://mirrors.dotsrc.org/fosdem/2023/K.4.601/graph_case_for_dag.webm

[eoss23-n-p]: 2023-kernel-ci-how-far-can-it-go.pdf
[eoss23-n-v]: https://www.youtube.com/watch?v=KD0Hk6PghPw

[devconf23-o-p]: 2023-reflections-on-cki-architecture.pdf
[devconf23-o-s]: 2023-reflections-on-cki-architecture.pdf
[devconf23-o-l]: https://docs.google.com/presentation/d/19G9I5UQNxa_G0vjBivo-z9bSDDyz6vBD0ZA1ChtURVE

[devconf23-a-p]: https://devconfcz2023.sched.com/event/1MYfa/the-many-ways-of-launching-aws-spot-instances
[devconf23-a-s]: 2023-the-many-ways-of-launching-aws-spot-instances.pdf
[devconf23-a-l]: https://docs.google.com/presentation/d/1UYaK6CxhV-BTaKvfFNBcWU5TquaYcXbCIiIQUqk1SLI

[devconf23-i-p]: https://devconfcz2023.sched.com/event/1MYhT/incident-management-for-small-service-teams
[devconf23-i-s]: 2023-incident-management-for-small-service-teams.pdf
[devconf23-i-l]: https://docs.google.com/presentation/d/1DyA_6QyJdiiBTaryolaNjoHwpC0Y9oWaUOUpTX-4fo8

[devconf22-p]: https://devconfcz2022.sched.com/event/siHv/advanced-gitlab-cicd-for-fun-and-profit
[devconf22-v]: https://www.youtube.com/watch?v=77913RmZUKk
[devconf22-s]: https://static.sched.com/hosted_files/devconfcz2022/79/DevConf.cz%202022%20-%20Advanced%20GitLab%20CI_CD%20for%20fun%20and%20profit.pdf
[devconf22-l]: https://docs.google.com/presentation/d/1-K93FEX8tAIlUHwQo4pAyEbp1czs59JYl7ZygVUrdMI

[fosdem22-p]: https://fosdem.org/2022/schedule/event/masking_known_issues_across_six_kernel_ci_systems
[fosdem22-v]: https://video.fosdem.org/2022/D.cicd/masking_known_issues_across_six_kernel_ci_systems.webm
[fosdem22-s]: https://fosdem.org/2022/schedule/event/masking_known_issues_across_six_kernel_ci_systems/attachments/slides/4882/export/events/attachments/masking_known_issues_across_six_kernel_ci_systems/slides/4882/Masking_known_issues_across_six_kernel_CI_systems_FOSDEM_2022.pdf

[devconf22m-v-p]: https://devconfczmini2022.sched.com/event/11lTy/keynote-panel-discussion-30-years-of-linux
[devconf22m-v-v]: https://www.youtube.com/watch?v=xob6ku7LB98

[devconf22m-m-p]: https://devconfczmini2022.sched.com/event/11l6G/open-source-requirements-for-services-bof
[devconf22m-m-v]: https://www.youtube.com/watch?v=BzlWqw_QW5w
[devconf22m-m-s]: https://static.sched.com/hosted_files/devconfczmini2022/84/Open%20Source%20Services%20Scorecard%20-%20engineering%20perspective.pdf
[devconf22m-m-l]: https://docs.google.com/presentation/d/1_mHzY_4cN1ekArFfHlK5YriTgQyql7aRhKfFd6D8320

[agile22-s]: 2022-agile-estimations.pdf
[agile22-l]: https://docs.google.com/presentation/d/1xQX2nxDLdV3YsxIP2Eyl84606H5Ys1kfkSzDzJsGASI

[devconf21-n-p]: https://devconfcz2021.sched.com/event/gmMt/unifying-kernel-test-reporting-with-kernelci
[devconf21-n-v]: https://www.youtube.com/watch?v=czHaqosibY0
[devconf21-n-s]: 2021-unifying-kernel-test-reporting-with-kernelci.pdf
[devconf21-n-l]: 2021-unifying-kernel-test-reporting-with-kernelci.odp
[devconf21-d-p]: https://devconfcz2021.sched.com/event/gmN5/rhel-kernel-workflow-using-gitlab
[devconf21-d-v]: https://www.youtube.com/watch?v=O9YRiXMskjU
[devconf21-d-s]: 2021-gitlab-kernel-workflow.pdf
[devconf21-d-l]: 2021-gitlab-kernel-workflow.odp
[devconf21-i-p]: https://devconfcz2021.sched.com/event/gmIL/from-jenkins-under-your-desk-to-resilient-service
[devconf21-i-v]: https://www.youtube.com/watch?v=C7FLD0FUnsY
[devconf21-i-s]: 2021-from-jenkins-under-your-desk-to-resilient-service.pdf
[devconf21-i-l]: 2021-from-jenkins-under-your-desk-to-resilient-service.odp
[devconf21-j-p]: https://devconfcz2021.sched.com/event/gmOm/upt-your-provisioning-of-linux-machines
[devconf21-j-v]: https://www.youtube.com/watch?v=gc2G8IqTIf0
[devconf21-j-s]: 2021-upt-your-provisioning-of-linux-machines.pdf
[devconf21-j-l]: 2021-upt-your-provisioning-of-linux-machines.odp
[cyborg21-i1-p]: 2021-cki-team-intro.pdf
[cyborg21-i1-s]: 2021-cki-team-intro.pdf
[cyborg21-i1-l]: 2021-cki-team-intro.odp
[cyborg21-i2-p]: 2021-how-do-you-run-your-service.pdf
[cyborg21-i2-s]: 2021-how-do-you-run-your-service.pdf
[cyborg21-i2-l]: 2021-how-do-you-run-your-service.odp
[cyborg21-i3-p]: 2021-how-do-you-hack-on-your-service.pdf
[cyborg21-i3-s]: 2021-how-do-you-hack-on-your-service.pdf
[cyborg21-i3-l]: 2021-how-do-you-hack-on-your-service.odp

[devconf20-j-p]: https://devconfcz2020a.sched.com/event/YOqY/7-ways-to-make-kernel-developers-like-cookies
[devconf20-j-v]: https://www.youtube.com/watch?v=hNxcuWkvzpo
[devconf20-j-s]: 2020-7-ways-to-make-kernel-developers-like-cookies.pdf

[devconf20-d-p]: https://devconfcz2020a.sched.com/event/YOvL
[devconf20-d-v]: https://www.youtube.com/watch?v=KhdDtGZc_jY
[devconf20-d-s]: 2020-plugging-into-the-red-hat-kernel-ci-ecosystem.pdf
[devconf20-d-l]: https://static.sched.com/hosted_files/devconfcz2020a/75/DevConf-RH-KCI.odp

[fosdem20-p]: https://archive.fosdem.org/2020/schedule/event/testing_abusing_gitlabci_test_kernel
[fosdem20-v]: https://www.youtube.com/watch?v=56QzKNxen0M
[fosdem20-s]: 2020-abusing-gitlab-ci-to-test-kernel-patches.pdf

[lp19-p]: https://lpc.events/event/4/contributions/287/
[lp19-v]: https://youtu.be/IM_k5fUsKhA
[lp19-s]: 2019-red-hat-joins-ci-party-brings-cookies.pdf

[devconf19-p]: https://devconfcz2019.sched.com/event/7f9283e801b3690bd347635c97bf876f
[devconf19-v]: https://www.youtube.com/watch?v=9KwDWsAqivo
[devconf19-s]: 2019-cookies-for-kernel-developers.pdf

[devconf18-p]: https://devconfcz2018.sched.com/event/DJXI/kernel-ci-how-red-hat-can-help
[devconf18-v]: https://www.youtube.com/watch?v=UYBu13CBmo8
