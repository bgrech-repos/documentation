---
title: "Welcoming clang built kernels into CKI family"
type: blog
date: "2021-01-21"
author: Veronika Kabatova
---

Up till now, CKI was only building kernels using `gcc` toolchains. This was
the standard, especially in the Fedora and RHEL land we're interacting with.
However, we are happy to announce that this is [no longer the case], and CKI
now allows building upstream kernels using `clang/llvm`! 🎉🥳

We have already enabled mainline building and testing for `x86_64`, `aarch64`
and `ppc64le`. Both `aarch64` and `x86_64` are using the full LLVM toolchain
(with `LLVM=1`), while `ppc64le` is compiled with `CC=clang`.

The results will be sent to the [clangbuiltlinux] mailing list and into the
upstream [kcidb] database, which is currently available at Kernel CI's staging
[dashboard].

## What's next?

We'll be moving `ppc64le` to the [full toolchain] as soon as the updated
versions required for it to work are available in Fedora, or kernel patches
making it work with older toolchains are in place. We are also excited to
include `s390x` into the family once it compiles with Fedora configurations.

Depending on our resources, we may be able to expand this configuration to
other trees later on.

[no longer the case]: https://gitlab.com/cki-project/pipeline-definition/-/merge_requests/1085
[clangbuiltlinux]: https://clangbuiltlinux.github.io/
[kcidb]: https://github.com/kernelci/kcidb
[dashboard]: https://staging.kernelci.org:3000/d/home/home?orgId=1
[full toolchain]: https://github.com/ClangBuiltLinux/linux/issues/811
