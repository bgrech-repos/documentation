---
title: "Join us at Devconf.cz in Brno!"
type: blog
date: "2019-01-04"
author: Major Hayden
tags:
  - presentations
---

![cki-talk-slide](cki-devconf-cz-talk-2019.png)

The CKI team will be together in Brno later this week for [Devconf.cz] and
team meetings. We have a talk on Friday afternoon called
["Cookies for Kernel Developers"] and we hope to see you there!

Our talk covers the origin of the project, where we are today, and where we
want to be in the future. We want more kernel developers and testers to be
involved as well. There will be plenty of information about all of that in
the talk.

Don't worry if you cannot make it to Brno this year. We will post the slides
here on the site shortly after the conference.

[Devconf.cz]: https://devconf.info/cz
["Cookies for Kernel Developers"]: https://devconfcz2019.sched.com/event/7f9283e801b3690bd347635c97bf876f
