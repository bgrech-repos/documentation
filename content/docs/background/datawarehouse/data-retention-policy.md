---
title: Datawarehouse Data Retention Policy
linkTitle: Data Retention Policy
weight: 10
---

## Purpose

The purpose of this document is to define the retention periods for each of
the personal data elements or records processed (ie., collected, used,
transmitted, shared, stored, archived) by the CKI Datawarehouse governed by the
requirements established in [Red Hat Corporate Records Management Policy] and
in compliance with “Data Retention” principle described in [Red Hat Global
Privacy Policy].

## Scope

The scope of this retention policy is limited to personal data elements or
records processed by the CKI Datawarehouse and do not apply to upstream or
downstream systems/services that could process the same personal data elements
or records. Data retention periods of the personal data processed by those
upstream or downstream systems/services shall be governed by the data retention
policies of those respective systems/services.

## Policy Statement

### Ownership & Responsibilities

All personal data elements or records collected, used, transmitted, shared,
stored, archived or otherwise processed by the CKI Datawarehouse is owned by
the CKI project and is responsible for establishing,
maintaining and enforcing this policy.

### Retention Periods

The following personal data elements or records are processed by the
CKI Datawarehouse and shall comply with the following retention periods. These
retention periods shall be enforced on any personal data transmitted to third
party vendors or service providers including various cloud or SaaS providers.

| What                            |  Purpose                                    | Max. Retention Period                                     |
|---------------------------------|---------------------------------------------|-----------------------------------------------------------|
| User ID                         | Authentication and authorization of users   | Until the termination of user’s relationship with Red Hat |
| Name and Email Address          | Communication with users                    | Until the termination of user’s relationship with Red Hat |
| Application or System Log Files | For logging events                          | 6 months                                                  |

### Storage

Maintenance and storage of personal data processed by this service shall be
governed by the requirements established in [Red Hat Corporate Records
Management Policy]. Any personal data in electronic format should be maintained
and stored in a Red Hat approved repository (or an approved cloud/service
provider) in compliance with [Data Encryption & Secure Key Management
Guidelines].

## Policy Compliance

### Compliance Measurement

Compliance with this data retention policy is mandatory. Compliance to this
policy is measured through various methods including but not limited to,
reports from available business tools, internal and external audits,
self-assessment, and/or feedback to the policy owner.

### Non-compliance & Exceptions

Any request to keep records beyond the maximum time period or retention
requirements must be justified and approved in writing by the policy owner.

## Revision History

| When       | Who                             | What                                 |
|------------|---------------------------------|--------------------------------------|
| 10/02/2021 | Martin Iñaki Malerba (imalerba) | Initial Policy drafted and published |

[Red Hat Corporate Records Management Policy]: https://source.redhat.com/departments/legal/corporatelegal/legalpolicies/compliance_folder/corporate_records_management_policy_pdfpdf
[Red Hat Global Privacy Policy]: https://source.redhat.com/groups/public/dataprivacy/global_data_protection__privacy_program_folder/global_privacy_policydocx
[Data Encryption & Secure Key Management Guidelines]: https://source.redhat.com/departments/it/it-information-security/wiki/data_encryption__secure_key_management_guidelines
