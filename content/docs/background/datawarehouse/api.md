---
title: Datawarehouse API
description: >-
    Documentation for Datawarehouse API can be found at
    https://cki-project.gitlab.io/datawarehouse/api/auth.html.
weight: 20
---

Most of Datawarehouse data can be accessed through its REST-like API.

Read more about it by clicking the
[link](https://cki-project.gitlab.io/datawarehouse/api/auth.html)
in the description.
