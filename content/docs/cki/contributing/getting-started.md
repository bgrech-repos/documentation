---
title: Getting started
description: How to start contributing to the code of the CKI project
weight: 10
---

## Red Hat associate

If you are a Red Hat associate getting started on the CKI team, start with the
[internal onboarding documentation].

## Development setup

### Linux distribution

To start contributing, you don't need any specific Linux distribution. While
we encourage everyone to develop on [Fedora Workstation] or [Fedora
Silverblue], this is not a requirement. Where Fedora/RHEL-specific software is
needed, container-based alternative workflows are provided.

### Python version

In general, you are encouraged to use the latest stable version.

For the [GitLab pipeline][com/pipeline-definition], code needs to be compatible
with the [minimal Python version][pipeline-python-version] across all pipeline
images.

For services outside the pipeline, code needs to be compatible
with the [Fedora Python version][fedora-python-version] used in all [container
images][fedora-container-version].

### Install dependencies

We need to install [direnv], [tox] and [git].

#### Fedora Workstation

All should be packaged for most distributions and
be installable with something like

```bash
sudo dnf install tox direnv git
```

#### RHEL 9

* Install Git

  ```bash
  sudo dnf install git
  ```

* Install tox from EPEL for RHEL 9

  ```bash
  sudo subscription-manager repos --enable codeready-builder-for-rhel-9-$(arch)-rpms
  sudo dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm
  sudo dnf install tox
  ```

* Install direnv from Fedora Koji

  ```bash
  sudo dnf install https://kojipkgs.fedoraproject.org/packages/direnv/2.32.1/6.fc39/$(arch)/direnv-2.32.1-6.fc39.$(arch).rpm
  ```

### Git config

Configure a global .gitignore file and add entries for direnv to it via
something like

```bash
IGNORE_FILE=$HOME/.config/git/excludes
git config --global core.excludesfile "${IGNORE_FILE}"
mkdir -p "${IGNORE_FILE%/*}"
echo "/.envrc" >> "${IGNORE_FILE}"
echo "/.direnv/" >> "${IGNORE_FILE}"
```

### Cloning CKI repositories and creating forks

For both GitLab instances, create `api` GitLab personal access tokens. The
tokens are going to be stored in the `COM_GITLAB_TOKEN_PERSONAL` and
`CEE_GITLAB_TOKEN_PERSONAL` environment variables below.

The following bash commands temporarily bootstrap cki-tools, and then use the
included `repo-manager` module to clone all repositories including forks and
setup pip:

```bash
mkdir ~/git-temp ~/git-cki
cd ~/git-temp
echo "export GITLAB_TOKENS='{\"gitlab.com\":\"COM_GITLAB_TOKEN_PERSONAL\", \"gitlab.cee.redhat.com\":\"CEE_GITLAB_TOKEN_PERSONAL\"}'" > .envrc
echo 'export COM_GITLAB_TOKEN_PERSONAL="your-secret-token-from-gitlab-com"' >> .envrc
echo 'export CEE_GITLAB_TOKEN_PERSONAL="your-secret-token-from-gitlab-cee"' >> .envrc
cp .envrc ~/git-cki/
git clone git@gitlab.com:cki-project/cki-tools
cd cki-tools
printf 'layout python python3.11\nsource_up\n' > .envrc
direnv allow
python3 -m pip install -e .
python3 -m cki.cki_tools.repo_manager --directory ~/git-cki --fork fork  --venv --force
cd ~/git-cki
rm -rf ~/git-temp
```

The venv setup depends on the ability to install the required dependencies.
This can fail if e.g. no wheels are available for your Python version or the
required development packages are not available to compile the requirements
from source.

For Fedora, you can fix most of these via

```bash
dnf install -y gcc krb5-devel libpq-devel openldap-devel python3-devel
```

### Linting and tests

#### Python-based projects

Linting, unit tests and code coverage checks are implemented in [cki\_lint.sh]
and can be invoked in a clean environment by running

```bash
tox
```

You can pass arguments to [cki\_lint.sh] following the [cki-lib README.md].
For example, with the environment variable `CKI_PYTEST_ARGS`,
the following code runs only on `test_name` in `tests/test_file_name.py`:

```bash
TOX_OVERRIDE=testenv.passenv=CKI_PYTEST_ARGS \
CKI_PYTEST_ARGS="tests/test_file_name.py::class_name::test_name" \
tox
```

To run tox with a different Python version, explicitly specify the Python
interpreter via

```bash
python3.9 -m tox
```

In the case of host problems, e.g. if no venv is available, the Python version
is too old or certificates are not setup correctly, tox can also be run in a
container via

```bash
podman run --rm --workdir /code --volume .:/code:z \
    quay.io/cki/cki-tools:production \
    tox --workdir .tox-container
```

To run only in a subset of the tests use `CKI_PYTEST_ARGS`:

```bash
podman run --rm --workdir /code --volume .:/code:z \
    --env TOX_OVERRIDE=testenv.passenv=CKI_PYTEST_ARGS \
    --env CKI_PYTEST_ARGS="tests/test_file_name.py[::class_name[::test_name]] ..." \
    quay.io/cki/cki-tools:production \
    tox --workdir .tox-container
```

Individual tests can also be run directly on the host via any of

```bash
python3 -m unittest tests.test_module
python3 -m unittest tests/test_file_name.py
pytest tests/test_file_name.py
```

The [cki-lib README.md] file contains the parameters for correct editor
integration of linters/fixers for CKI code.

#### Pipeline code

In [com/pipeline-definition], the linting and testing can be invoked locally via

```bash
./lint.sh
./tests.sh
```

or in a container via

```bash
podman run --rm --workdir /code --volume .:/code:z \
    quay.io/cki/cki-tools:production \
    ./lint.sh
podman run --rm --workdir /code --volume .:/code:z \
    quay.io/cki/builder-rawhide:production \
    ./tests.sh
```

[internal onboarding documentation]: https://documentation.internal.cki-project.org/l/onboarding
[Fedora Workstation]: https://getfedora.org/nl/workstation/
[Fedora Silverblue]: https://silverblue.fedoraproject.org/
[direnv]: https://direnv.net/
[git]: https://git-scm.com/
[tox]: https://tox.readthedocs.io/en/latest/
[cki\_lint.sh]: https://gitlab.com/cki-project/cki-lib/-/blob/main/cki_lint.sh
[cki-lib README.md]: https://gitlab.com/cki-project/cki-lib/-/blob/main/README.md
[com/pipeline-definition]: https://gitlab.com/cki-project/pipeline-definition
[pipeline-python-version]: https://gitlab.com/cki-project/pipeline-definition/-/blob/main/spec/dependencies_spec.sh
[fedora-python-version]: https://packages.fedoraproject.org/search?query=python3
[fedora-container-version]: https://gitlab.com/cki-project/containers/-/blob/main/includes/setup-from-fedora#L5
