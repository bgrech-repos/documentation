---
title: Enabling scratch build testing
description: >
    How to enable Koji/Brew scratch build testing for a new RHEL major version
---

## Problem

Testing of Koji/Brew scratch builds is handled by several different pieces
across CKI.

## Steps

1. Find the relevant trigger configuration in [brew.yaml] in the
   [pipeline-data] repository.

2. Enable scratch build testing by adding `.test_scratch: 'true'`.

3. Add a suitable RPM release filter like `rpm_release: 'el9.*\.test\.cki\.'`
   for RHEL 9.

## Background

Scratch build testing is triggered by [koji-trigger] based on the
`.test_scratch` key. Whether a CKI pipeline should be triggered for a specific
build is configured via the `rpm_release` regular expression.

The test set selection defaults to the set configured via the `test_set`
trigger variable in `brew.yaml` in the internal pipeline-data repository.

For custom test set selection, the `KERNEL_RELEASE` parsing happens in the
[setup stage].

[brew.yaml]: https://gitlab.com/cki-project/pipeline-data/-/blob/main/brew.yaml
[pipeline-data]: https://gitlab.com/cki-project/pipeline-data/
[koji-trigger]: /l/koji-trigger
[setup stage]: https://gitlab.com/cki-project/pipeline-definition/-/blob/main/pipeline/stages/setup.yml
