---
title: Purging a merge request from GitLab
linkTitle: Removing an MR
description: >
    How to remove all data associated with a merge request
aliases: [/l/removing-mr]
---

## Problem

There is a merge request that shouldn't exist.

The merge request and all associated data needs to be removed from all affected
systems.

{{% include "internal-buckets.md" %}}

## Steps

1. Don't panic!

1. On the merge request page, switch to the `Pipelines` tab and cancel all
   running pipelines. Open each pipeline in a separate browser tab and make
   sure all child pipelines are properly cancelled as well.

1. Determine all spawned child pipelines. This can be done via something like

   ```python
   from cki_lib import gitlab

   gl_instance, gl_mr = gitlab.parse_gitlab_url(mr_url)

   pipeline_urls = [
       gl_bridge.downstream_pipeline['web_url']
       for gl_pipeline in gl_mr.pipelines.list(iterator=True)
       for gl_bridge in gl_instance.projects.get(gl_pipeline.project_id, lazy=True)
           .pipelines.get(gl_pipeline.id, lazy=True).bridges.list(iterator=True)
       if gl_bridge.downstream_pipeline
   ]
   print('\n'.join(pipeline_urls))
   ```

   For each pipeline, a [DataWarehouse] checkout like [redhat:807685102] should
   exist.

1. For the S3 buckets above, artifacts are stored per GitLab pipeline ID. With
   the `aws_s3.sh` helper in [deployment-all], artifacts can be deleted via
   something like

   ```bash
   for bucket_prefix in "${bucket_prefixes[@]}"; do
       for pipeline_id in "${pipeline_ids[@]}"; do
         ./aws_s3.sh "${bucket_prefix}" \
           s3 rm --dryrun --recursive "s3://BUCKETPREFIX/${pipeline_id}/"
       done
   done
   ```

   Remove `--dryrun` once you verified that only the expected objects are going
   to get deleted.

1. Delete each child pipeline via the `Delete` button on the pipeline page in
   the GitLab web interface.

1. [Log into the DataWarehouse production instance][dw-prod], and delete the
   checkouts and connected objects via something like

   ```python
   KCIDBCheckout.objects.filter(id__in=[f'redhat:{i}' for i in pipeline_ids]).delete()
   ```

1. Delete the merge request via the `Delete` button that becomes visible when
   editing a merge request as a project owner.

[DataWarehouse]: https://datawarehouse.cki-project.org/

[redhat:807685102]: https://datawarehouse.cki-project.org/kcidb/checkouts/redhat:807685102
[deployment-all]: https://gitlab.cee.redhat.com/cki-project/deployment-all

[dw-prod]: ../datawarehouse-handbook.md
