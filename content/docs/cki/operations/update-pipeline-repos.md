---
title: Updating pipeline repositories
description: >
  How to update the pipeline repositories
---

## Problem

You want to update the GitLab CI/CD configuration in the [Pipeline repositories].

## Setup

In all refs in all pipeline repositories, the `.gitlab-ci.yml` file should be updated.
This will require careful actions with `commit --amend` and `git branch -f` to modify the branches.
It is possible to update the repository through fork and merge requests. However, it would be
a lot of work for not much gain, as a description of the resulting merge commits still
needs to be changed. In that case, it would be necessary to file one MR per branch, and then manually
change all the descriptions when merging.

It is advisable to make the changes with a temporary access token for the cki-bot account.
This will result in commits being authored by that account, reducing user confusion.

- Create a temporary access token (only valid a couple of hours) for the cki-bot account.
  Credentials can be found in the `credentials` repository.

- Update `.git/config` with a valid content.

  For example:

  ```apacheconf
  [remote "origin"]
    url = url = https://cki-bot:token@gitlab.com/redhat/red-hat-ci-tools/kernel/cki-internal-pipelines/cki-trusted-contributors.git
    fetch = +refs/heads/*:refs/remotes/origin/*
  [branch "main"]
    remote = origin
    merge = refs/heads/main
    rebase = true
  [user]
    name = CKI Bot
    email = 3953733-cki-bot@users.noreply.gitlab.com
  ```

- Clone the repository that you want to update via something like

  ```bash
  git clone git@gitlab.com:redhat/red-hat-ci-tools/kernel/cki-internal-pipelines/cki-trusted-contributors.git
  ```

- Create a file `update_pipelines.sh` with the following content and make it executable:

  ```bash
  #!/bin/bash

  set -euo pipefail

  git fetch --all -j 4 --prune
  git checkout main
  readarray -t branches < <(git branch -r --format '%(refname:short)' | grep -v '^origin/\(HEAD\|main\)$')
  for branch in "${branches[@]}"; do
      echo "${branch}"
      (
          git branch --track --force "${branch##*/}" "${branch}"
          git checkout "${branch##*/}"
          git reset --hard "${branch}"
      ) &> /dev/null
      if ! cmp -s .gitlab-ci.yml .gitlab-ci.template.yml; then
          cp .gitlab-ci.template.yml .gitlab-ci.yml
          git diff
          git commit -a --amend --no-edit --reset-author
          # git push origin --force
      fi
  done
  ```

- Copy `.gitlab-ci.yml` to `.gitlab-ci.template.yml`, and modify it with the required changes.

- Execute the `update_pipelines.sh` script.

- Once you've verified that the changes are applied as expected,
  uncomment the `git push` line and re-execute the script.

[Pipeline repositories]: https://gitlab.com/redhat/red-hat-ci-tools/kernel
