---
title: DataWarehouse LDAP Groups
description: Synchronizing LDAP groups with DataWarehouse permissions
---

Datawarehouse uses a system of permissions (table based) and authorizations
(object based) to limit the read and write access users have across the platform.

For more information about permissions and authorizations, check DataWarehouse
[permissions documentation].

To manage these permissions it's possible to synchronize them with LDAP groups.
On our DataWarehouse instance, these groups are mirrored from Rover groups.
This means that to give an user certain permission it's necessary to add them
to the correct Rover group and wait for these to be synchronized (currently this
happens hourly).

## Groups

The following groups are currently defined:

- [cki-datawarehouse-internal-write]: CKI DataWarehouse users with 'write'
  authorization in 'internal' policy

Being member of this group gives the user **authorization** to **write objects**
related to the `internal` policy.

- [cki-datawarehouse-public-write]: CKI DataWarehouse users with 'write'
  authorization in 'public' policy

Being member of this group gives the user **authorization** to **write objects**
related to the `public` policy. These are already readable by anyone.

- [cki-kernel-tests-reviewers]: CKI DataWarehouse users with triaging
  permissions

Being member of this group gives the user **permission** to **write tables**
related to triaging issues.

## Internal access

For users to be able to read internal checkouts and issues, the users need to
be member of one of the following groups:

- `bugzilla-redhat`
- `linux-eng-pe`

## Granting Triaging permissions

For users to be able to perform write operations -for example triaging- they
need to have both *permission* and *authorization*.

The `cki-kernel-tests-reviewers` group grants users belonging to it
*permission* to edit the issue-related tables, but that doesn't grant the user
the *authorization* necessary to edit the objects.

### Triaging Public Issues

To grant users permission to only triage public checkouts and issues, the
following groups are necessary:

- `cki-kernel-tests-reviewers`
- `cki-datawarehouse-public-write`

### Triaging Internal Issues

To grant users permission to only triage internal checkouts and issues, the
following groups are necessary:

- `bugzilla-redhat`
- `cki-kernel-tests-reviewers`
- `cki-datawarehouse-internal-write`

### Triaging All Issues

To grant users permission to triage all checkouts and issues, the
following groups are necessary:

- `bugzilla-redhat`
- `cki-kernel-tests-reviewers`
- `cki-datawarehouse-internal-write`
- `cki-datawarehouse-public-write`

[permissions documentation]: https://cki-project.gitlab.io/datawarehouse/permissions.html
[cki-datawarehouse-internal-write]: https://rover.redhat.com/groups/group/cki-datawarehouse-internal-write
[cki-datawarehouse-public-write]: https://rover.redhat.com/groups/group/cki-datawarehouse-public-write
[cki-kernel-tests-reviewers]: https://rover.redhat.com/groups/group/cki-kernel-tests-reviewers
