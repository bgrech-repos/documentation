# Documentation of the CKI project

This repository contains the source for the CKI [documentation website].

[All contributions are welcome!][contributing]

[documentation website]: https://cki-project.org/
[contributing]: https://cki-project.org/l/writing-documentation
